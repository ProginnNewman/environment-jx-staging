CHART_REPO := http://ac60dc935a42e11e9b7e0024b8391079-1605233461.cn-northwest-1.elb.amazonaws.com.cn:15031
DIR := "env"
NAMESPACE := "staging"
OS := $(shell uname)

build: clean
	rm -rf requirements.lock
	helm version
	helm init
	helm repo add releases ${CHART_REPO}
	helm repo add jenkins-x http://100.68.116.93:8080
	helm dependency build ${DIR}
	helm lint ${DIR}

install: 
	helm upgrade ${NAMESPACE} ${DIR} --install --namespace ${NAMESPACE} --debug

delete:
	helm delete --purge ${NAMESPACE}  --namespace ${NAMESPACE}

clean:


